def compress(string):
    # if string is null or a single character than it returns the string
    if(len(string) <= 1):
        return string
    # initialization with the first character of string with the count = 1
    cur_char = string[0]
    cur_size = 1
    answer = ""
    for i in range(1, len(string)):
        # if the character is same as previous character than increment in count
        if(string[i] == cur_char):
            cur_size += 1
        # if the character value changes than append the string with character and its count and initialize new character with count = 1
        else:
            answer = answer + cur_char + str(cur_size)
            cur_char = string[i]
            cur_size = 1
    answer = answer + cur_char + str(cur_size)
    return answer


string = input("input stringing:")  # takes input for the string
print(compress(string))

'''
 Time Complexity
 ->the timecomlexity of the above function is O(n)
 ->as there is one for loop who checks every character in string in
  linear fashion
'''
