class graph:
	def identify_router(self, string):
		# split the string to get the nodes of the graph
		string = string.split(' -> ')
		gra = []
		graph = {}
		# making a graph using dictionary
		for i in range(len(string) - 1):
			gra.append([string[i], string[i + 1]])
		for i in gra:
			if(i[0] in graph.keys()):
				graph.update({i[0]: graph[i[0]] + [i[1]]})
			else:
				graph.update({i[0]: [i[1]]})
		max, ans = 0, []
		# making seperate dictionaries to store count of incoming links and outgoing links for each node
		incoming = {}
		outgoing = {}
		for i in gra:
			if i[0] in outgoing:
				outgoing.update({i[0]: outgoing[i[0]] + 1})
			else:
				outgoing.update({i[0]: 1})
			if i[1] in incoming:
				incoming.update({i[1]: incoming[i[1]] + 1})
			else:
				incoming.update({i[1]: 1})
		#the loop counts maximum links out of nodes that are present in both oncoming and outgoing dictionary as well as the nodes that are present only in outgoing dictioanry.
		for i in outgoing:
			if i in incoming:
				if(incoming[i] + outgoing[i] > max):
					max = incoming[i] + outgoing[i]
					ans = [i]
				elif(incoming[i] + outgoing[i] == max):
					ans.append(i)
			else:
				if(outgoing[i] > max):
					max = outgoing[i]
					ans = [i]
				elif(outgoing[i] == max):
					ans.append(i)
		#the loop counts maximum links out of nodes that are only present in inocming dictionary
		for i in incoming:
			if i not in incoming:
				if(incoming[i] > max):
					max = outgoing[i]
					ans = [i]
				elif(incoming[i] == max):
					ans.append(i)
		#returns the array in which the maximum links was found.
		return ans


obj = graph()
ip = input("input string:") #takes input for the string
ans = obj.identify_router(ip)
print("Network Failure Point:" , *ans, sep = ",")

'''
Time complexity of the identify_router() is O(n^2)
as there are nested loops 
'''
